function isTokenValid(token) {
    try {
        const payloadBase64Url = token.split('.')[1];
        const payloadBase64 = payloadBase64Url.replace('-', '+').replace('_', '/');
        const payload = JSON.parse(window.atob(payloadBase64));

        const currentTime = Math.floor(Date.now() / 1000);
        return payload.exp > currentTime;
    } catch (error) {
        return false;  // If there's any error, the token is not valid
    }
}

function myFunction() {
    let x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

document.addEventListener("DOMContentLoaded", function() {
    const token = localStorage.getItem('userToken');

    document.getElementById('getStartedButton').addEventListener('click', function() {
        window.location.href = `../public/html/signup.html`; // Integrated with the new invoke URL
    });

    document.getElementById('signUpButton').addEventListener('click', function() {
        window.location.href = `../public/html/signup.html`; // Integrated with the new invoke URL
    });

    document.getElementById('signInButton').addEventListener('click', function() {
        window.location.href = `../public/html/login.html`; // Integrated with the new invoke URL
    });




        document.getElementById('makeSchedule').addEventListener('click', function (event) {
            event.preventDefault();

            if (!token || !isTokenValid(token)) {
                window.location.href = '../public/html/login.html';
                return;
            }
            window.location.href = 'public/html/schema.html';
        });

        document.getElementById('scheduleView').addEventListener('click', function (event) {
            event.preventDefault();

            if (!token || !isTokenValid(token)) {
                window.location.href = '../public/html/login.html';
                return;
            }
            window.location.href = 'public/html/schemaView.html';
        });

        document.getElementById('trainwellLogo').addEventListener('click', function (event) {
            event.preventDefault();
            window.location.href = '/index.html';
        });

        document.getElementById('servicesButton').addEventListener('click', function (event) {
            event.preventDefault();

            if (!token || !isTokenValid(token)) {
                window.location.href = '../public/html/login.html';
                return;
            }
            window.location.href = 'public/html/services.html';
        });

        document.getElementById('profilePage').addEventListener('click', function (event) {
            event.preventDefault();

            if (!token || !isTokenValid(token)) {
                window.location.href = '../public/html/login.html';
                return;
            }
            window.location.href = 'public/html/profile.html';
        });
});

module.exports = isTokenValid()