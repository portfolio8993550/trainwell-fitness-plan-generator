document.addEventListener('DOMContentLoaded', function() {
    const BASE_URL = 'https://gml8agl1v2.execute-api.us-east-1.amazonaws.com/firstTest-3CORS';



    document.getElementById('signup-form').addEventListener('submit', function (event) {
        event.preventDefault();

        let firstName = document.getElementById('firstName').value;
        let lastName = document.getElementById('lastName').value;
        let sex = document.getElementById('sex').value;
        let email = document.getElementById('email').value;
        let password = document.getElementById('password').value;

        let requestBody = {
            profileData: {
                firstName: firstName,
                lastName: lastName,
                sex: sex
            },
            email: email,
            password: password,
        };


        fetch(`${BASE_URL}/signup`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        })
            .then((response) => {
                if (!response.ok) {
                    return response.json().then((data) => Promise.reject(data));
                }
                return response.json();
            })
            .then((data) => {
                if (data.success) {
                    console.log('Signup successful');
                    window.location.href = './login.html';
                }
            })
            .catch((error) => {
                console.error('Signup failed:', error.message);
                // You can show an error message to the user here
            });
    })
})