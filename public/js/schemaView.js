function isTokenValid(token) {
    try {
        const payloadBase64Url = token.split('.')[1];
        const payloadBase64 = payloadBase64Url.replace('-', '+').replace('_', '/');
        const payload = JSON.parse(window.atob(payloadBase64));

        const currentTime = Math.floor(Date.now() / 1000);
        return payload.exp > currentTime;
    } catch (error) {
        return false;  // If there's any error, the token is not valid
    }
}
document.addEventListener('DOMContentLoaded', async () => {
    const BASE_URL = 'https://gml8agl1v2.execute-api.us-east-1.amazonaws.com/firstTest-3CORS';
    const token = localStorage.getItem('userToken');
    if (!token || !isTokenValid(token)) {
        alert("not logged in")
        window.location.href = '../public/html/login.html';
        return;
    }

    const schedulesList = document.getElementById('schedules-list');
    const scheduleDetails = document.getElementById('schedule-details');

    // Fetch the current user
    let userId;
    const userResponse = await fetch(`${BASE_URL}/currentUser`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
    const userData = await userResponse.json();
    if (userData.user) {
        userId = userData.user._id;
    } else {
        console.error('Failed to fetch current user');
        return;
    }

    // Call the function to load schedules
    loadSchedules(userId);

    async function loadSchedules(userId) {
        // Fetch schedules from the server
        const response = await fetch(`${BASE_URL}/getSchedules`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer'+ token
            },
            body: JSON.stringify({ userId })
        });

        if (!response.ok) {
            console.error('Failed to load schedules');
            return;
        }

        const schedules = await response.json();

        // Render the list of schedules in the sidebar
        for (const schedule of schedules) {
            const scheduleElem = renderScheduleListItem(schedule);
            schedulesList.appendChild(scheduleElem);
        }

        // By default, show the details of the first schedule
        if (schedules.length > 0) {
            renderScheduleDetails(schedules[0]);
        }
    }

    function renderScheduleListItem(schedule) {
        const listItem = document.createElement('li');
        const dateCreated = new Date(schedule.dateCreated);
        const formattedDate = dateCreated.toLocaleString();

        listItem.textContent = `${schedule.fitnessGoal} made on: ${formattedDate}`;

        // Add edit button
        const editButton = document.createElement('button');
        editButton.textContent = "Edit";
        editButton.addEventListener('click', () => {
            editSchedule(schedule);
        });
        listItem.appendChild(editButton);

        // Add remove button
        const removeButton = document.createElement('button');
        removeButton.textContent = "Remove";
        removeButton.addEventListener('click', () => {
            confirmRemove(schedule);
        });
        listItem.appendChild(removeButton);

        listItem.style.cursor = 'pointer';
        listItem.addEventListener('click', () => {
            renderScheduleDetails(schedule);
        });

        return listItem;
    }
    function editSchedule(schedule) {
        const modal = document.getElementById('editModal');
        const fitnessGoalInput = document.getElementById('fitnessGoalInput');

        // Set the current content
        fitnessGoalInput.value = schedule.fitnessGoal;

        // Show the modal
        modal.style.display = 'block';

        // On "Save" button click
        document.getElementById('saveEditButton').addEventListener('click', function() {
            const editedText = fitnessGoalInput.value;
            // For now, just log the edited text. In real-life scenarios, send this to the server.
            console.log(editedText);

            // Close the modal
            modal.style.display = 'none';
        });
    }


    function confirmRemove(schedule) {
        const isConfirmed = window.confirm(`Are you sure you want to remove the schedule: ${schedule.fitnessGoal}?`);
        if (isConfirmed) {
            removeSchedule(schedule);
        }
    }

    function removeSchedule(schedule) {
        // Implement the removal logic here. For now, we just remove it from the UI.
        const schedulesList = document.getElementById('schedules-list');
        const scheduleItems = schedulesList.children;
        for (let item of scheduleItems) {
            if (item.textContent.includes(schedule.fitnessGoal)) {
                schedulesList.removeChild(item);
                break;
            }
        }
        // NOTE: You probably want to send a request to your server to actually delete this schedule from the database.
    }


    function renderScheduleDetails(schedule) {
        // Clear out the current schedule details
        scheduleDetails.innerHTML = '';

        // Add each day to the schedule details
        for (const day in schedule.schedule) {
            if (day !== "Note") {
                const dayElem = renderDay(day, schedule.schedule[day]);
                scheduleDetails.appendChild(dayElem);
            }
        }

        // Add the note
        const noteElem = document.createElement('p');
        noteElem.textContent = `Note: ${schedule.schedule.Note}`;
        scheduleDetails.appendChild(noteElem);
    }

    function renderDay(day, dayData) {
        const dayElem = document.createElement('div');
        dayElem.classList.add('day');

        // Day heading
        const dayHeading = document.createElement('h3');
        dayHeading.textContent = day;
        dayElem.appendChild(dayHeading);

        // Type
        const typeElem = document.createElement('p');
        typeElem.textContent = `Type: ${dayData.type}`;
        dayElem.appendChild(typeElem);

        // Total Time
        const totalTimeElem = document.createElement('p');
        totalTimeElem.textContent = `Total Time: ${dayData.totalTime}`;
        dayElem.appendChild(totalTimeElem);

        // Exercises
        const exercisesHeading = document.createElement('h4');
        exercisesHeading.textContent = 'Exercises:';
        dayElem.appendChild(exercisesHeading);

        const exercisesList = document.createElement('ul');
        for (const exercise of dayData.exercises) {
            const exerciseElem = document.createElement('li');
            exerciseElem.textContent = `${exercise.name} (${exercise.sets})`;
            exercisesList.appendChild(exerciseElem);
        }
        dayElem.appendChild(exercisesList);

        return dayElem;
    }
});

// bij schedules list naam en goal erbij ;;;;;;;;;;;;;;;;;;;; done
// bij services 1 schema maken
// bij aankoop kun je je profiel 1 keer updaten en 2 schemas maken
// refund bij no progress
// one time payment 4,99
// stripe fixen




//alternatieven voor oefeningen door chatGPT