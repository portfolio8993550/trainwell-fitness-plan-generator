function isTokenValid(token) {
    try {
        const payloadBase64Url = token.split('.')[1];
        const payloadBase64 = payloadBase64Url.replace('-', '+').replace('_', '/');
        const payload = JSON.parse(window.atob(payloadBase64));

        const currentTime = Math.floor(Date.now() / 1000);
        return payload.exp > currentTime;
    } catch (error) {
        return false;  // If there's any error, the token is not valid
    }
}

document.addEventListener('DOMContentLoaded', function() {
    const BASE_URL = 'https://gml8agl1v2.execute-api.us-east-1.amazonaws.com/firstTest-3CORS';
    const token = localStorage.getItem('userToken');
    if (!token || !isTokenValid(token)) {
        alert("not logged in")
        window.location.href = '../public/html/login.html';
        return;
    }
console.log(token)

    fetch(`${BASE_URL}/getProfileData`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
        .then(response => response.json())
        .then(data => {console.log(data)
            if (data.success) {
                console.log(data)
                const {age, height, weight, bodyFat, activityLevel, fitnessGoal, fitnessLevel, trainingDuration, selectedValues } = data.profileData || {};
                document.getElementById("age").value = age || "";
                document.getElementById("height").value = height || "";
                document.getElementById("weight").value = weight || "";
                document.getElementById("body-fat").value = bodyFat || "";
                document.getElementById("activity-level").value = activityLevel || "";
                document.getElementById("fitness-goal").value = fitnessGoal || "";
                document.getElementById("fitness-level").value = fitnessLevel || "";
                document.getElementById("time-duration").value = trainingDuration || "";
                // setting checked status for checkboxes based on fetched values
                let trainingTypeCheckboxes = document.querySelectorAll('input[name="trainingType"]');
                trainingTypeCheckboxes.forEach(checkbox => {
                    checkbox.checked = selectedValues.includes(checkbox.value);
                });
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    // Rest of your code

    document.getElementById("updateButton").addEventListener("click", function(event){
        event.preventDefault();
        let age = document.getElementById("age").value;
        let height = document.getElementById("height").value;
        let weight = document.getElementById("weight").value;
        let bodyFat = document.getElementById("body-fat").value;
        let activityLevel = document.getElementById("activity-level").value;
        let fitnessGoal = document.getElementById("fitness-goal").value;
        let fitnessLevel = document.getElementById("fitness-level").value;
        let trainingDuration = document.getElementById("time-duration").value;
        let trainingTypeCheckboxes = document.querySelectorAll('input[name="trainingType"]:checked');
        let selectedValues = Array.from(trainingTypeCheckboxes).map(cb => cb.value);

        let data = {
            age,
            height,
            weight,
            bodyFat,
            activityLevel,
            fitnessGoal,
            fitnessLevel,
            trainingDuration,
            selectedValues
        };

    fetch(`${BASE_URL}/updateProfile`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({ profileData: data }),
    })
    window.location.reload()
});


})