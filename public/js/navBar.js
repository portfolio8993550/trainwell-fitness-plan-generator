function myFunction() {
    let x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
document.addEventListener('DOMContentLoaded', function() {

    document.getElementById('makeSchedule').addEventListener('click', function (event) {
        event.preventDefault();
        window.location.href = './schema.html';
    });

    document.getElementById('scheduleView').addEventListener('click', function (event) {
        event.preventDefault();
        window.location.href = './schemaView.html';
    });

    document.getElementById('trainwellLogo').addEventListener('click', function (event) {
        event.preventDefault();
        window.location.href = '../../index.html';  // Assuming that the login page is your home page. Change if necessary.
    });

    document.getElementById('servicesButton').addEventListener('click', function (event) {
        event.preventDefault();
        window.location.href = './services.html';  // Note: You mentioned 'services' as a "to do", so ensure the filename is correct when it's done.
    });

    document.getElementById('profilePage').addEventListener('click', function (event) {
        event.preventDefault();
        window.location.href = './profile.html';
    });
    document.getElementById('signOutButton').addEventListener('click', function (event) {
        event.preventDefault();
        localStorage.removeItem('userToken');
        window.location.href = '../../index.html';  // Assuming that the login page is your home page. Change if necessary.
    });
});
