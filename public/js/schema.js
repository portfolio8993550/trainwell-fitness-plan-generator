function isTokenValid(token) {
    try {
        const payloadBase64Url = token.split('.')[1];
        const payloadBase64 = payloadBase64Url.replace('-', '+').replace('_', '/');
        const payload = JSON.parse(window.atob(payloadBase64));

        const currentTime = Math.floor(Date.now() / 1000);
        return payload.exp > currentTime;
    } catch (error) {
        return false;  // If there's any error, the token is not valid
    }
}

document.addEventListener('DOMContentLoaded', function() {
    // Your existing code here...

    // Get references to HTML elements
    const schemaRadios = document.querySelectorAll('input[name="schema"]');
    const requestArea = document.querySelector('.form-group textarea[name="request"]');
    const formGroupRequest = requestArea.closest('.form-group');
    const formInsertPoint = document.getElementById('schema-form');

    // Function to toggle display of request textarea and additional input
    function toggleRequestAndAdditionalInput() {
        const checkedRadio = document.querySelector('input[name="schema"]:checked');
        if (checkedRadio) {
            if (checkedRadio.value === 'workout') {
                addOrReplaceAdditionalInput('What do you plan to achieve with this workout schedule?', 'workout-input');
            } else if (checkedRadio.value === 'food') {
                addOrReplaceAdditionalInput('What is your goal with this food schedule?', 'food-input');
            }
            formGroupRequest.style.display = 'block';
        } else {
            formGroupRequest.style.display = 'none';
        }
    }

    // Function to add or replace the additional input field
    function addOrReplaceAdditionalInput(labelText, inputId) {
        let additionalInput = document.getElementById('additional-input');
        if (additionalInput) {
            additionalInput.remove();
        }
        additionalInput = document.createElement('div');
        additionalInput.id = 'additional-input';
        additionalInput.className = 'form-group';
        additionalInput.innerHTML = `
        <label for="${inputId}">${labelText}</label>
        <input type="text" id="${inputId}" name="${inputId}">
    `;
        formInsertPoint.insertBefore(additionalInput, formGroupRequest);
    }

    // Initial toggle based on the default state
    toggleRequestAndAdditionalInput();

    // Event listeners for radio buttons
    schemaRadios.forEach(radio => {
        radio.addEventListener('change', toggleRequestAndAdditionalInput);
    });

    let userId;
    let userData = {}; // Define a userData variable here
    const BASE_URL = 'https://gml8agl1v2.execute-api.us-east-1.amazonaws.com/firstTest-3CORS';
    const token = localStorage.getItem('userToken');
    if (!token || !isTokenValid(token)) {
        alert("not logged in")
        window.location.href = '../public/html/login.html';
        return;
    }

    fetch(`${BASE_URL}/currentUser`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
        .then(response => response.json())
        .then(data => {
            if (data.user) {
                userId = data.user._id
                userData = data.user
                console.log(data.user._id);
                console.log(data.user)
                let userProfileInfo = document.getElementById("userProfileInfo");
                const stringSelectedValues= data.user.profileData.selectedValues.join(', ')
                userProfileInfo.innerHTML = `
    <p>Email: ${data.user.email}</p>
    <p>First Name: ${data.user.profileData.firstName}</p>
    <p>Last Name: ${data.user.profileData.lastName}</p>
    <p>Sex: ${data.user.profileData.sex}</p>
    <p>Age: ${data.user.profileData.age}</p>
    <p>Height: ${data.user.profileData.height} cm</p>
    <p>Weight: ${data.user.profileData.weight}</p>
    <p>Body Fat: ${data.user.profileData.bodyFat}</p>
    <p>Activity Level: ${data.user.profileData.activityLevel}</p>
    <p>Fitness Goal: ${data.user.profileData.fitnessGoal}</p>
    <p>Fitness Level: ${data.user.profileData.fitnessLevel}</p>
    <p>Training Duration: ${data.user.profileData.trainingDuration} minutes<br><br></p>
    <p>Training Types: <br><br> ${stringSelectedValues}</p> 
`;
                console.log(data)
            } else {
                console.log("error");
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    document.getElementById('getSchemaButton').addEventListener('click', function (event) {
        event.preventDefault();

        // 1. Extract user profile data from the innerHTML
        let userProfileInfo = document.getElementById("userProfileInfo");
        let userProfileText = userProfileInfo.innerText;

        // Here we extract the data values from the displayed user profile
        let email = userData.email;
        let sex = userData.profileData.sex;
        let age = userData.profileData.age;
        let height = userData.profileData.height;
        let weight = userData.profileData.weight;
        let bodyFat = userData.profileData.bodyFat;
        let activityLevel = userData.profileData.activityLevel;
        let fitnessGoal = userData.profileData.fitnessGoal;
        let fitnessLevel = userData.profileData.fitnessLevel;
        let trainingDuration = userData.profileData.trainingDuration;
        let selectedValues = userData.profileData.selectedValues.join(', ');

        let schemaType = document.querySelector('input[name="schema"]:checked').value;
        let request = document.getElementById("request").value;
        let includePreviousPreferences = document.getElementById("previous-preferences").checked;

        let prompt = `Dear AI,
Could you please create a weekly workout schedule for a user? The schedule should strictly adhere to the following structure:

- Day 1 to Day 7: Type of Training
- Exercise 1: Exercise Name (Number of sets x Number of reps OR Duration in minutes)
- Exercise 2: Exercise Name (Number of sets x Number of reps OR Duration in minutes)
- And so on for each exercise...
- Total Time: Total time for the workout in minutes
- For rest days, please use "Rest and Recovery" as the type of training.

Please ensure that the output strictly follows this format. The schedule will be personalized according to the following user's information:

The user is a ${sex} individual, ${age} years old, with a height of ${height} cm, a weight of ${weight} kg, and a body fat percentage of ${bodyFat}%. The user has an activity level of ${activityLevel} and a fitness goal to ${fitnessGoal}. Their current level of fitness is ${fitnessLevel}. They can train for ${trainingDuration} minutes a day and would prefer exercises of the following types: ${selectedValues}.

Also, please include a note at the end with helpful tips and reminders for the user. This schedule should primarily focus on achieving the user's fitness goal but shouldn't be overly restricted by it. Thank you.`;

        if (request) {
            prompt += ` ${request}`;
        }

        if (includePreviousPreferences && profileData.preferences) {
            prompt += ` Please also take into account the user's previous preferences: ${profileData.preferences}`;
        }


        fetch(`${BASE_URL}/find-complexity`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({ prompt, schemaType, userId }),
        })
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    console.log(data);

                    // Display the generated prompt
                    alert("Generated Prompt: " + data);

                    // Redirect to the schedules page:
                    window.location.href = './schemaView.html';
                } else {
                    // Handle errors, such as displaying an error message to the user
                    console.error(data.message); // Log the specific error message from the server
                    alert('Error generating prompt: ' + data.message);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });

    });




});


// 2 kolommen voor food en schedule meer layout natuurlijk
// integratie met agenda. API
