document.addEventListener('DOMContentLoaded', function() {
    const BASE_URL = 'https://9cxx5trsu1.execute-api.us-east-1.amazonaws.com/test';
    document.getElementById('login-form').addEventListener('submit', function(event) {
        event.preventDefault();

        let email = document.getElementById('email').value;
        let password = document.getElementById('password').value;

        let requestBody = {
            email: email,
            password: password,
        };

        // Making the fetch POST request to the loginCheck route
        fetch(`${BASE_URL}/loginCheck`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        })
            .then((response) => {
                if (!response.ok) {
                    return response.json().then((data) => Promise.reject(data));
                }
                return response.json();
            })
            .then((data) => {
                if (data.success) {
                    console.log('Login successful');
                    localStorage.setItem('userToken', data.token);
                    window.location.href = '../html/profile.html';
                }
            })
            .catch((error) => {
                console.error('Login failed:', error.message);
                // You can show an error message to the user here
            });
    });
});
